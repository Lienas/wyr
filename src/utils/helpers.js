/**
 * formats a timestamp to german date :-)
 * @param timestamp
 * @returns {string}
 */
export function formatDate(timestamp) {
    const d = new Date(timestamp)
    const time = d.toLocaleTimeString('de-DE')
    return d.toLocaleDateString() + " " + time
}

/**
 * shortens a question depending on the length
 * gt 5: first 3 and last 2 char are removed
 * lt 5: full string is returned
 * @param questionText
 * @returns {string}
 */
export function questionTeaser(questionText) {
    const length = questionText.length
    if (length <= 5) {
        return questionText
    } else {
        return `...${questionText.slice(3, length - 2)}...`
    }
}

/**
 * destructures question and authedUser to simplify the component
 * @param question
 * @param authedUser
 * @returns {{date: (string|null), optionOneVoted: (*|boolean), optionOneVotes: (*|number), optionTwo: *, optionTwoVotes: (*|number), optionOne: *}}
 */
export function formatResult(question, authedUser) {
    return {
        optionOne: question ? question.optionOne.text : "n/a",
        optionTwo: question ? question.optionTwo.text : "n/a",
        optionOneVotes: question ? question.optionOne.votes.length : 0,
        optionTwoVotes: question ? question.optionTwo.votes.length : 0,
        optionOneVoted: question ? question.optionOne.votes.includes(authedUser) : false,
        date: question ? formatDate(question.timestamp) : null
    }
}

/**
 * determines whether the user answered the given question
 * @param question
 * @param user
 * @returns {boolean}
 */
export function questionAnsweredByUser(question, user) {
    let votes =
        question.optionOne.votes.concat(question.optionTwo.votes)
    return votes.includes(user);
}

/**
 * determines if the user is logged in
 * @param authedUser:string
 * @returns {boolean}
 */
export function isAuthenticated(authedUser) {
    return authedUser !== null
}


/**
 * calculates the total of asked and answered questions
 * and returns an array sorted by the total count
 * @param ranks {{userdId:string, asked:string, answered: string}}
 * @returns sorted array{{}[]}
 */
function addTotalAndSort(ranks) {

    //calc total
    let ranksWithTotal = {}
    for (const userId in ranks) {
        if (ranks.hasOwnProperty(userId)) {
            ranksWithTotal[userId] = {
                ...ranks[userId],
                total: (ranks[userId].asked ? ranks[userId].asked : 0)
                    + (ranks[userId].answered ? ranks[userId].answered : 0)
            }
        }
    }

    // convert to array an sort
    return Object.keys(ranksWithTotal)
        .map(r => ranksWithTotal[r])
        .sort((a, b) => b.total - a.total) //.sort((a,b) => a.total > b.total)
}

/**
 * iterates through questions and calculates
 * asked and answered questions
 * @param questions
 * @returns {*[]}
 */
export function getRanking(questions) {
    let ranks = {}

    /**
     * count the answers for votes per user
     * @param votes
     */
    function getAnswered(votes) {
        for (const userId of votes) {
            ranks[userId] = {
                ...ranks[userId],
                answered: (ranks[userId] && ranks[userId].answered)
                    ? ranks[userId].answered + 1
                    : 1
            }
        }
    }

    for (const questionId in questions) {

        if (questions.hasOwnProperty(questionId)) {
            let question = questions[questionId];
            ranks[question.author] =
                {
                    ...ranks[question.author],
                    user: question.author,
                    asked: (ranks[question.author] && ranks[question.author].asked)
                        ? ranks[question.author].asked + 1
                        : 1
                }

            // get answered for optionOne
            let votes = question.optionOne.votes
            getAnswered(votes);

            // get answered for optionTwo
            votes = question.optionTwo.votes
            getAnswered(votes);
        }
    }

    return addTotalAndSort(ranks);
}


