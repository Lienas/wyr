import {_getQuestions, _getUsers, _saveQuestion, _saveQuestionAnswer} from "./_DATA";

export function getInitialData() {
    return Promise.all(
        [
            _getUsers(),
            _getQuestions()
        ]).then(([users, questions]) => ({
        users,
        questions
    }))
}

export function saveVote(vote) {
    console.log("API _saveQuestionAnswer called ")
    return _saveQuestionAnswer(vote)
}

/**
 *
 * @param question
 *        = {optionOneText: string, optionTwoText: string, authorId: string}
 * @returns {Promise | Promise<question>}
 *           question = {`id`, `author`, `optionOne`, `optionTwo`, `timestamp`}
 */
export function saveQuestion(question) {
    return _saveQuestion(question)
}