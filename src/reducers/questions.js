import {RECEIVE_QUESTIONS, ADD_VOTE, ADD_QUESTION} from "../actions/questions";

export default function questions(state = {}, action) {
    switch (action.type) {
        case RECEIVE_QUESTIONS:
            return {
                ...state,
                ...action.questions
            }
        case ADD_VOTE:
            const {vote} = action
            console.log("ADD VOTE: ", state[vote.qid][vote.answer].votes)

            return {
                ...state,
                [vote.qid]: {
                    ...state[vote.qid],
                    [vote.answer]: {
                        ...state[vote.qid][vote.answer],
                        votes: state[vote.qid][vote.answer].votes.concat([vote.authedUser])
                    }
                }
            }
        case ADD_QUESTION:
            const {question} = action
            return {
                ...state,
                [question.id]: {
                    id: question.id,
                    timestamp: question.timestamp,
                    optionOne: question.optionOne,
                    optionTwo: question.optionTwo,
                    author: question.author
                }

            }

        default:
            return state
    }

}