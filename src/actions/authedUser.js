export const SET_AUTHED_USER = 'SET_AUTHED_USER';

/**
 * action to set the logged in user
 * @param userId
 * @returns {{id: *, type: string}}
 */
export function setAuthedUser(userId) {
    return {
        type: SET_AUTHED_USER,
        id: userId
    }
}
