import {getInitialData} from "../utils/api";
import {receiveQuestions} from "./questions";
import {receiveUsers} from "./users";
import {showLoading, hideLoading} from "react-redux-loading";

/**
 * loads initial data from API
 * @returns {function(*): Promise<{questions: *, users: *}>}
 */
export function handleInitialData() {
    return (dispatch) => {
        dispatch(showLoading())
        return getInitialData()
            .then(({questions, users}) => {
                    dispatch(receiveUsers(users))
                    dispatch(receiveQuestions(questions))
                    dispatch(hideLoading())
                }
            )
    }
}