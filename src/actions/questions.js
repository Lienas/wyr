import {hideLoading, showLoading} from "react-redux-loading";
import {saveQuestion, saveVote} from "../utils/api";

export const RECEIVE_QUESTIONS = 'RECEIVE_QUESTIONS'
export const ADD_VOTE = 'ADD_VOTE'
export const ADD_QUESTION = 'ADD_QUESTION'

/**
 * get the questions from the store
 * @param questions
 * @returns {{questions: *, type: string}}
 */
export function receiveQuestions(questions) {
    return{
     type: RECEIVE_QUESTIONS,
     questions
    }
}

/**
 * action for dispatching a vote
 * @param vote = object{authedUser, question, answer}
 * @returns {{type: string, vote: *}}
 */
function addVote(vote) {
    return {
        type: ADD_VOTE,
        vote
    }
}

/**
 * action for adding a new question
 * @param question
 *        =  {optionOne:string,
 *            optionTwo:string,
 *            authorId:string}
 * @returns {{question: *, type: string}}
 */
function addQuestion(question) {
    return {
        type:ADD_QUESTION,
        question
    }

}

/**
 * Adds a vote for a user to a question
 * @param vote: object {authedUser, quid, answer}
 * with authedUser =
 *      id of the user
 *      quid = id of the question
 *      answer = optionOne or optionTwo
 * @returns {function(*): Promise<T>}
 */
export function handleAddVote(vote) {
    return (dispatch) => {
        dispatch(showLoading())
        return saveVote(vote)
            .then(() => {
                dispatch(addVote(vote))
            })
            .then(() => dispatch(hideLoading()))
    }
}

export function handleAddQuestion (question) {
    return (dispatch) => {
        dispatch(showLoading())

        return saveQuestion(question)
            .then((question) => {
                console.log("handleAddQuestion:", question)
                dispatch(addQuestion(question))
            })
            .then(()=>dispatch(hideLoading()))
    }
}