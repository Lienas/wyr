export const RECEIVE_USERS = 'RECEIVE_USERS'

/**
 * action to get the users from store
 * @param users
 * @returns {{type: string, users: *}}
 */
export function receiveUsers(users) {
    return {
        type: RECEIVE_USERS,
        users
    }

}