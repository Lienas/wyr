import React from 'react';
import {Redirect, Route} from "react-router-dom";
import {isAuthenticated} from "../utils/helpers";

export const PrivateRoute = ({component: Component, authedUser, path, ...rest}) => (
    <Route
        path={path}
        {...rest}
        render={(props) => {
            return isAuthenticated(authedUser) ? (
                <Component {...props} />
            ) : (
                <Redirect to={{
                    pathname: '/login',
                    state: {
                        prevLocation: props.location.pathname
                    },
                }}/>
            )
        }}/>
)