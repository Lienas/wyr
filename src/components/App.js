import React, {Fragment} from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'
import {connect} from 'react-redux'
import '../App.css';
import NavBar from "./Navbar";
import Dashboard from "./Dashboard";
import {handleInitialData} from "../actions/shared";
import LoadingBar from "react-redux-loading";
import QuestionDetail from "./QuestionDetail";
import NotFound_404 from "./NotFound_404";
import Login from "./Login";
import {PrivateRoute} from "./PrivateRoute";
import NewQuestion from "./NewQuestion";
import Leaderboard from "./Leaderboard";


class App extends React.Component {

    componentDidMount() {
        this.props.dispatch(handleInitialData())
    }

    render() {
        const authedUser = this.props.authedUser;
        return (
            <Router>
                <Fragment>
                    <LoadingBar/>
                    <div className="App container">
                        <NavBar/>
                        {this.props.loading === true
                            ? null
                            : <Switch>
                                <PrivateRoute
                                    path='/'
                                    exact
                                    component={Dashboard}
                                    authedUser={authedUser}/>
                                <PrivateRoute path='/add'
                                              exact
                                              component={NewQuestion}
                                              authedUser={authedUser}/>
                                <PrivateRoute path='/leaderboard'
                                              exact
                                              component={Leaderboard}
                                              authedUser={authedUser}/>
                                <Route path="/login"
                                       component={Login}/>
                                <PrivateRoute path='/questions/:questionsId'
                                              component={QuestionDetail}
                                              authedUser={authedUser}/>
                                <Route path='/404'
                                       component={NotFound_404}/>
                                {/*if nothing matches display "not found" page*/}
                                <Redirect to="/404"/>
                            </Switch>
                        }
                    </div>
                </Fragment>
            </Router>
        );
    }
}

function mapStateToProps({questions, authedUser}) {
    return {
        loading: questions === null,
        authedUser
    }
}

export default connect(mapStateToProps)(App);
