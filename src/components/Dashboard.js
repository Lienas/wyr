import React from "react";
import {connect} from "react-redux";
import Question from './Question'
import {questionAnsweredByUser} from "../utils/helpers";
import {Link} from "react-router-dom";

class Dashboard extends React.Component {

    state = {
        showAnswered: false
    }

    handleQuestionStatus = (showAnswered) => {
        this.setState({showAnswered: showAnswered})
    }

    render() {

        const {questions} = this.props
        const {showAnswered} = this.state
        const filteredQuestions = questions
            .filter((question) => question.answered === showAnswered)

        return (
            <div className="mt-5">
                <ul className="nav nav-tabs">
                    <li className="nav-item">
                        <button
                            className={setClasses(!showAnswered)}
                            onClick={() => this.handleQuestionStatus(false)}>
                            Unanswered Questions
                        </button>
                    </li>
                    <li className="nav-item">
                        <button
                            className={setClasses(showAnswered)}
                            onClick={() => this.handleQuestionStatus(true)}>
                            Answered Questions
                        </button>
                    </li>

                </ul>

                {
                    filteredQuestions.length > 0
                        ? filteredQuestions
                            .map((question) =>
                                <Question
                                    id={question.id}
                                    answered={showAnswered}
                                    key={question.id}/>)
                        : <div className="alert alert-info mt-5">
                            you have answered all, questions, <br/>
                            click <Link to="/add">here</Link> to post a new question
                        </div>

                }
            </div>
        )

    }
}

function setClasses(isActive) {
    let classes = "nav-link"
    classes = classes + (isActive ? " active" : "")
    return classes
}

function mapStateToProps({authedUser, questions}) {
    return {
        questions: Object.keys(questions)
            .sort((a, b) =>
                questions[b].timestamp - questions[a].timestamp)

            .map((id) => {
                let newObj = {
                    id: id,
                    answered: questionAnsweredByUser(questions[id], authedUser)
                }

                return (Object.assign(newObj))
            })
    }
}

export default connect(mapStateToProps)(Dashboard)