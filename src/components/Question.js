import React from "react";
import {connect} from "react-redux";
import {formatDate, questionTeaser} from "../utils/helpers";
import {Link} from "react-router-dom";


class Question extends React.Component {
    render() {
        const {teaser, date, user, answered, id, authedUser} = this.props
        const {name, avatarURL} = user

        return <div className="card text-left mb-3 mt-3">
            <div className="card-header">
                <strong> {user.id === authedUser ?
                    <span className="text-primary">YOU</span> : name}</strong> asked <span
                className="small">({date})</span>
            </div>


            <div className="card-body">
                <div className="row">
                    <div className="col-3">
                        <img className="img-thumbnail"
                             src={avatarURL}
                             alt={user.name}
                             title={user.name}/>
                    </div>
                    <div className="card-text col">
                        <h5 className="card-title">
                            Would-you rather?
                        </h5>
                        <p>{teaser}</p>
                        <Link to={`/questions/${id}`} className="btn btn-primary">
                            {answered ? "View Results" : "Vote "}
                        </Link>
                    </div>

                </div>
            </div>

            <div className="card-footer text-right">
                <p className="small text-muted m-0">id = {id}</p>
            </div>
        </div>;
    }
}

function mapStateToProps({questions, users, authedUser}, props) {

    const {id, answered} = props
    const optionOne = questions[id].optionOne.text

    return {
        id: id,
        teaser: questionTeaser(optionOne),
        date: formatDate(questions[id].timestamp),
        user: users[questions[id].author],
        answered,
        authedUser
    }
}

export default connect(mapStateToProps)(Question)