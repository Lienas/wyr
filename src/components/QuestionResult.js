import React from "react";
import * as PropTypes from "prop-types";
import {formatResult} from "../utils/helpers";

export function QuestionResult(props) {

    const {user, question, authedUser} = props
    const {optionOne, optionTwo, optionOneVotes, optionTwoVotes, optionOneVoted, date} = formatResult(question, authedUser)
    const totalVotes = optionOneVotes + optionTwoVotes

    return (
        <div className="card text-left mb-3 mt-3">
            <div className="card-header">
                {user.name} asked <span className="small">({date})</span>
            </div>
            <div className="card-body">

                <div className="card-body">
                    <div className="row">
                        <div className="col-3">
                            <img className="img-thumbnail"
                                 src={user.avatarURL}
                                 alt={user.name}
                                 title={user.name}/>
                        </div>
                        <div className="card-text col">
                            <h5 className="card-title">
                                Would-you rather?
                            </h5>
                            <h6 className="mt-3">
                                {optionOne}: <small>{optionOneVotes}/{totalVotes} votes</small>
                                {optionOneVoted &&
                                <span className="badge badge-success ml-2">your vote</span>}
                            </h6>
                            <div className="progress">
                                <div className="progress-bar"
                                     aria-valuenow="100"
                                     aria-valuemin="0"
                                     aria-valuemax="100"
                                     style={{width: optionOneVotes / totalVotes * 100 + "%"}}>
                                    {(optionOneVotes / totalVotes * 100).toFixed(1)} %
                                </div>
                            </div>

                            <h6 className="mt-3">
                                {optionTwo}: <small>{optionTwoVotes}/{totalVotes} votes</small>
                                {!optionOneVoted &&
                                <span className="badge badge-success ml-2">your vote</span>}
                            </h6>
                            <div className="progress">
                                <div className="progress-bar"
                                     aria-valuenow="100"
                                     aria-valuemin="0"
                                     aria-valuemax="100"
                                     style={{width: optionTwoVotes / totalVotes * 100 + "%"}}>
                                    {(optionTwoVotes / totalVotes * 100).toFixed(1)} %
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

QuestionResult.propTypes = {
    question: PropTypes.object,
    user: PropTypes.object,
    authedUser: PropTypes.string,
    answered: PropTypes.bool
};