import React from "react";
import {connect} from "react-redux";
import {setAuthedUser} from "../actions/authedUser";
import {withRouter} from "react-router-dom";

class UserInfo extends React.Component {

    handleLogout = (e) => {
        e.preventDefault()
        const {dispatch, history} = this.props
        dispatch(setAuthedUser(null))
        history.push('/login')
    }

    render() {
        const {authedUserObj} = this.props
        return (
            authedUserObj
                ? <div className="my-2 my-lg-0">
                    <img
                        src={authedUserObj.avatarURL}
                        className="img-thumbnail float-left mr-2"
                        style={{width: 35}}
                        alt={authedUserObj.name}
                    />
                    <form className="form-inline">
                        <h6 className="text-white mt-2">Hello! {authedUserObj.name}</h6>
                        <button className="btn btn-sm btn-secondary ml-2" onClick={this.handleLogout}>Logout</button>
                    </form>

                </div>
                : null
        )
    }
}

function mapStateToProps({authedUser, users}) {
    return (
        authedUser
            ? {authedUserObj: users[authedUser]}
            : {}
    )

}

export default withRouter(connect(mapStateToProps)(UserInfo))