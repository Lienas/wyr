import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {setAuthedUser} from "../actions/authedUser";
import {withRouter} from "react-router-dom";
import Select from "react-select";

class Login extends Component {

    state = {
        userId: "na"
    }

    handleSelectionChanges = (e) => {
        this.setState({
            userId: e.value
        })
    }

    handleLogin = (e) => {
        e.preventDefault()
        const {dispatch, history, location} = this.props
        const {userId} = this.state
        const redirectTarget = location.state ? location.state.prevLocation : '/'

        dispatch(setAuthedUser(userId))
        history.push(redirectTarget)
    }

    render() {

        const disabled = this.state.userId === 'na'
        const {users} = this.props

        let options = Object.keys(users).map((userId) => {

                const user = users[userId];
                return {
                    value: userId,
                    label: <Fragment>
                        <img src={user.avatarURL} alt={userId} style={{width: 20 + 'px'}}/>
                        <span className="text-primary ml-2">{user.name}</span>
                    </Fragment>
                }
            }
        )

        return (

            <div className="card mt-5">
                <div className="card-header">
                    <h3>Welcome to the <span className="text-primary">Would You Rather</span> App</h3>
                    <p className="small">Please log in to continue</p>
                </div>
                <div className="card-body align-middle">
                    <div className="row">
                        <form className="col-4 text-left mt-auto mb-auto">
                            <div className="form-group">
                                <label>Username</label>
                                <Select
                                    options={options}
                                    isClearable={true}
                                    onChange={this.handleSelectionChanges}/>
                                <small className="form text text-muted">Choose your name from the
                                    list</small>
                            </div>
                            <button className="btn btn-primary"
                                    disabled={disabled}
                                    onClick={this.handleLogin}>Login
                            </button>
                        </form>
                        <div className="col-8">
                            <img src="/logo.svg" alt="react"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps({users}) {
    return {
        users: users
    }

}

export default withRouter(connect(mapStateToProps)(Login));