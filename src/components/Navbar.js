import React from "react";
import UserInfo from "./UserInfo";
import {NavLink} from "react-router-dom";

export default function Navbar() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark mt-1">
            <h3 className="navbar-brand">Would You Rather...</h3>
            <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                    <NavLink to={'/'} activeClassName="active" className="nav-link">Home</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to={'/add'} activeClassName="active" className="nav-link">New Question</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to={'/leaderboard'} activeClassName="active" className="nav-link">Leaderboard</NavLink>
                </li>
            </ul>
            <UserInfo/>
        </nav>
    )
}