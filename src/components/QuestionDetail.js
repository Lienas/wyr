import React from "react";
import {connect} from "react-redux";
import {QuestionResult} from "./QuestionResult";
import QuestionVote from "./QuestionVote";
import {questionAnsweredByUser} from "../utils/helpers";
import {Redirect} from 'react-router-dom'

class QuestionDetail extends React.Component {

    //prevent from unnecessary rendering
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.authedUser !== null
    }

    render() {
        const {
            question,
            user,
            authedUser,
            answered
        } = this.props

        if (question) {
            return (
                answered
                    ? <QuestionResult
                        question={question}
                        user={user}
                        authedUser={authedUser}/>
                    : <QuestionVote
                        id={question.id}
                        user={user}
                        authedUser={authedUser}/>
            )
        } else {
            return <Redirect to="/404"/>
        }

    }
}

function mapStateToProps({questions, users, authedUser}, props) {

    const {questionsId} = props.match.params
    const question = questions[questionsId]

    return {
        questionsId,
        question,
        user: question ? users[question.author] : "n/a",
        authedUser,
        answered: question ? questionAnsweredByUser(question, authedUser) : false
    }
}


export default connect(mapStateToProps)(QuestionDetail)