import React from "react";
import {connect} from "react-redux";
import {formatDate} from "../utils/helpers";
import {handleAddVote} from "../actions/questions";

class QuestionVote extends React.Component {

    state = {
        vote: null
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const {authedUser, questionId, dispatch} = this.props
        const {vote} = this.state

        vote !== null
            ? dispatch(handleAddVote({authedUser, qid: questionId, answer: vote}))
            : alert("nothing selected")
    }

    handleChange = (e) => {
        const vote = e.target.value
        this.setState({
            vote
        })
    }

    render() {

        const {optionOne, optionTwo, user, date} = this.props
        return (
            <div className="card text-left mb-3 mt-3">
                <div className="card-header">
                    {user.name} asked <span className="small">({date})</span>
                </div>
                <div className="card-text col">

                    <div className="card-body">
                        <div className="row">

                            <div className="col-3">
                                <img
                                    src={user.avatarURL}
                                    alt={user.name}
                                    title={user.name}
                                    className="img-thumbnail"/>
                            </div>

                            <div className="col text-body">
                                <h5 className="card-title">
                                    Would-you rather?
                                </h5>
                                <form onSubmit={(e) => this.handleSubmit(e)}>

                                    <div className="form-check">
                                        <input type="radio"
                                               name="vote"
                                               className="form-check-input"
                                               id="optionOne"
                                               value="optionOne"
                                               onChange={this.handleChange}/>

                                        <label htmlFor="optionOne"
                                               className="form-check-label">
                                            {optionOne}
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input type="radio"
                                               name="vote"
                                               className="form-check-input"
                                               id="optionTwo"
                                               value="optionTwo"
                                               onChange={this.handleChange}/>
                                        <label htmlFor="optionTwo"
                                               className="form-check-label">
                                            {optionTwo}
                                        </label>
                                    </div>
                                    <button
                                        type="submit"
                                        className="btn btn-primary mt-3"
                                        disabled={!this.state.vote}>Vote
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps({questions, users}, props) {
    const {id, authedUser} = props
    const question = questions[id]
    const optionOne = question.optionOne.text
    const optionTwo = question.optionTwo.text

    return {
        questionId: question.id,
        optionOne: optionOne,
        optionTwo: optionTwo,
        authedUser,
        date: formatDate(question.timestamp)
    }


}

export default connect(mapStateToProps)(QuestionVote)