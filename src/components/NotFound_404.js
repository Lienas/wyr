import React from 'react';
import {Link} from "react-router-dom";

function NotFound_404() {

    return (
        <div className="mt-5">
            <img src="/img/error-404.svg" alt={"page not found"}/>
            <p className="alert alert-info">UPS, something went wrong! Your requested page was not found!</p>
            <Link to="/">Go to Home </Link>
        </div>
    );
}

export default NotFound_404;