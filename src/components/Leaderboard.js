import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {getRanking} from "../utils/helpers";

class Leaderboard extends Component {
    render() {
        const {ranking, users} = this.props
        let pos = 0
        return (
            <Fragment>
                <h1 className="mt-5 mb-5">
                    Leader-Board
                </h1>
                <table className="table table-hover">
                    <thead className="thead-light">
                    <tr className="th">
                        <th scope="col">#</th>
                        <th scope="col" className="text-left">user</th>
                        <th scope="col">asked</th>
                        <th scope="col">answered</th>
                        <th scope="col">total</th>
                    </tr>
                    </thead>

                    <tbody>{
                        ranking.map((rank) => {
                                pos++
                                let user = users[rank.user]
                                return (
                                    <tr key={rank.user}>
                                        <th scope="row">{pos}</th>
                                        <td className="text-left">
                                            <img
                                                src={user.avatarURL}
                                                alt={user.name}
                                                style={{width: 30 + 'px'}}
                                                className="img-thumbnail mr-2"
                                            /> {user.name}
                                        </td>
                                        <td>{rank.asked}</td>
                                        <td>{rank.answered}</td>
                                        <td><strong>{rank.total}</strong></td>
                                    </tr>
                                )
                            }
                        )
                    }
                    </tbody>
                </table>
            </Fragment>
        );
    }
}


function mapStateToProps({questions, users}) {

    const ranking = getRanking(questions)
    console.log("Ranking", ranking)

    return {
        ranking,
        users
    }

}

export default connect(mapStateToProps)(Leaderboard);