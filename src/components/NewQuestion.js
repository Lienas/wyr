import React, {Component} from 'react';
import {connect} from "react-redux";
import {handleAddQuestion} from "../actions/questions";
import {withRouter} from "react-router-dom";

class NewQuestion extends Component {
    state = {
        optionOne: "",
        optionTwo: ""
    }

    handleChange = (e) => {
        const option = e.target.id
        const value = e.target.value
        this.setState(() => {
                return {
                    [option]: value
                }
            }
        )
    }

    handleSubmit = () => {
        const {dispatch, authedUser, history} = this.props
        const question = {
            author: authedUser,
            optionOneText: this.state.optionOne,
            optionTwoText: this.state.optionTwo
        }
        dispatch(handleAddQuestion(question))
        history.push('/')

    }

    render() {
        const canSubmit = this.state.optionOne && this.state.optionTwo
        return (
            <div className="card text-left mb-3 mt-3">
                <div className="card-header">
                    Create a new Question
                </div>
                <div className="card-body">
                    <h2 className="mb-5">Would you rather ...</h2>
                    <div className="form-group mb-5">
                        <input
                            className="form-control mb-3"
                            placeholder="Enter option one"
                            id="optionOne"
                            onChange={this.handleChange}/>
                        <div className="small text-center">or</div>
                        <input
                            className="form-control mt-3"
                            placeholder="Enter option two"
                            id="optionTwo"
                            onChange={this.handleChange}/>
                    </div>
                    <button
                        className="btn btn-outline-primary"
                        disabled={!canSubmit}
                        onClick={this.handleSubmit}>Submit
                    </button>
                </div>
            </div>
        );
    }
}

function mapStateToProps({authedUser}) {
    return {
        authedUser
    }

}

export default withRouter(connect(mapStateToProps)(NewQuestion));